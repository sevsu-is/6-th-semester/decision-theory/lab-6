﻿// LR_6_TPR.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <iomanip>
using namespace std;
const int m = 4, n = 5;

double A1[m][m] = {
    {1,0.5,0.75,0.25},
    {2,1,1.5,0.5},
    {1.33,0.67,1,0.33},
    {4,2,3,1}
};

double A21[n][n] = {
    {1,0.5,0.75,0.25,1.25},
    {2,1,1.5,0.5,2.50},
    {1.33,0.67,1,0.33,1.67},
    {4,2,3,1,5},
    {0.8,0.4,0.6,0.2,1}
},
A22[n][n] = {
    {1,5,3,2,6},
    {0.2,1,0.6,0.4,1.2},
    {0.33,1.67,1,0.67,2},
    {0.5,2.5,1.5,1,3},
    {0.17,0.83,0.5,0.33,1}
},
A23[n][n] = {
    {1,4,7,8,3},
    {0.25,1,1.75,2,0.75},
    {0.14,0.57,1,1.14,0.43},
    {0.13,0.5,0.88,1,0.38},
    {0.33,1.33,2.33,2.67,1}
},
A24[n][n] = {
    {1,2,1,1,1},
    {1.0/2.0,1,1,1,1},
    {1,1,1,1,1},
    {1,1,1,1,1},
    {1,1,1,1,1}
};

double c1[m] = { 0,0,0,0 }, c[n] = { 0,0,0,0,0 };

double W1[m] = { 0,0,0,0 }, W_itog[m] = {0,0,0,0}, sum = 0, sobstven,is;
int i, j;

double w21[n] = { 0,0,0,0,0 }, w22[n] = { 0,0,0,0,0 }, w23[n] = { 0,0,0,0,0 }, w24[n] = { 0,0,0,0,0 };
double wp1[m], wp2[m], wp3[m], wp4[m], wp5[m];

double WP(double wp[m],int index);
double metod1(double A[5][5], double w[5], int index);



int main()
{
    setlocale(LC_ALL, "Russian");

    cout << "Матрица A1" << endl;

    cout << setw(7) << "K1";
    cout << setw(7) << "K2";
    cout << setw(7) << "K3";
    cout << setw(7) << "K4"<<endl;

    for (i = 0; i < m; i++) {
        cout << "K" << i + 1;
        for (j = 0; j < m; j++) {
            cout << setw(6) << A1[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;


    sum = 0;
    for (i = 0; i < m; i++) {
        for (j = 0; j < m; j++) {
            W1[i] = W1[i] + A1[i][j];
        }
        sum = sum + W1[i];
    }
 
    cout << "w1 = { ";
    for (i = 0; i < m; i++) {
        W1[i] = W1[i] / sum;
        cout << W1[i] << " ";
    }
    cout<<"}" << endl;

    cout << endl;

    sum = 0;
    for (i = 0; i < m; i++) {
        sum = sum + W1[i];
    }

    cout << "Сумма элементов вектора w должна быть равна 1" << endl << "Выполним проверку:" << endl;
    for (i = 0; i < m; i++) {
        cout << W1[i] << " + ";
        if (i+1 == m) {
            cout << W1[i] << " = ";
        }
    }

    cout << sum << endl;

    
    
    sum = 0;
    float temp = 0;
    for (i = 0; i < m; i++) {
        temp = 0;
        for (j = 0; j < m; j++) {
            temp += A1[i][j] * W1[j];
        }
        c1[i] = temp;
    }

    for (i = 0; i < m; i++) {
        W_itog[i] = c1[i] / W1[i];
        sum += W_itog[i]/m;
    }

    //cout << "sum =" << sum << endl;
    cout << endl;
    sobstven = sum;
    cout <<"Собственное значение матрицы А1: "<<sobstven << endl;
    is = (sobstven - m) / (m - 1);
    cout << "Индекс согласованности: " << is << endl;
    cout << endl;
    cout << endl;

    metod1(A21, w21, 1);
    metod1(A22, w22, 2);
    metod1(A23, w23, 3);
    metod1(A24, w24, 4);
    cout << "Вектор всех значений характеристик для каждой альтернативы" << endl<<endl;
    WP(wp1,0);
    WP(wp2,1);
    WP(wp3,2);
    WP(wp4,3);
    WP(wp5,4);
    cout << endl;

    double D[n] = {0,0,0,0,0};

    for (i = 0; i < m; i++) {
        D[0] += wp1[i] * W1[i];
        D[1] += wp2[i] * W1[i];
        D[2] += wp3[i] * W1[i];
        D[3] += wp4[i] * W1[i];
        D[4] += wp5[i] * W1[i];
    }

    
    for (i = 0; i < n; i++) {
        cout << "D" << i + 1 << " = ";
        cout <<setw(6) << D[i] << " ";
    }
    cout << endl;
    cout << endl;
  
        double max = 0; int ind;
    for (i = 0; i < n; i++) {
        if (D[i] > max) {
            max = D[i];
            ind = i;
        }
    }
    cout << "Отсюда следует, что " << ind + 1 << " альтернатива является наилучшей" << endl;
    cout <<"D"<<ind+1<<" = " << max << endl;

}


double WP(double wp[m], int index) {
   
        wp[0] = w21[index];
        wp[0 + 1] = w22[index];
        wp[0 + 2] = w23[index];
        wp[0 + 3] = w24[index];


       
        cout << "Для альт" << index + 1<<" = {";
        for (i = 0; i < m; i++) {
            cout << setw(10) << wp[i] << " ";
        }
        cout <<"}" << endl;

    
    return wp[m];
}

double metod1(double A[5][5], double w[5], int index) {
    double  wt[n] = { 0,0,0,0,0 };
    sum = 0;

    cout << "Матрица А2" << index << endl;
    cout << setw(12) << "Альт1";
    cout << setw(11) << "Альт2";
    cout << setw(11) << "Альт3";
    cout << setw(11) << "Альт4";
    cout << setw(11) << "Альт5" << endl;

    for (i = 0; i < n; i++) {
        cout << "Альт" << i + 1;
        for (j = 0; j < n; j++) {
            cout << setw(9) << A[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            w[i] = w[i] + A[i][j];
        }
        sum = sum + w[i];
    }

    cout << "w" << index + 1 << " = {";
    for (i = 0; i < n; i++) {
        w[i] = w[i] / sum;
        cout << w[i] << " ";
    }
    cout << "}" << endl;

    sum = 0;
    for (i = 0; i < m; i++) {
        sum = sum + W1[i];
    }
    cout << endl;
    cout << "Сумма элементов вектора w должна быть равна 1" << endl << "Выполним проверку:" << endl;
    for (i = 0; i < n; i++) {
        cout << w[i] << " + ";
        if (i + 1 == n) {
            cout << w[i] << " = ";
        }
    }

    cout << sum << endl;
    cout << endl;

    
    float temp = 0;
    for (i = 0; i < n; i++) {
         temp = 0;
        for (j = 0; j < n; j++) {
            temp += A[i][j] * w[j];
        }
        c[i] = temp;
    }

    sum = 0;
    for (i = 0; i < n; i++) {
        wt[i] = c[i] / w[i];
        sum += wt[i]/n;
    }
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            c[i] = 0;
        }
    }


    sobstven = sum;
    cout << "Собственное значение матрицы А2" << index << " : " << sobstven << endl;
    is = (sobstven - n) / (n - 1);
    cout << "Индекс согласованности: " << is << endl;
    cout << endl;
    cout << endl;

    return w[i];


}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
